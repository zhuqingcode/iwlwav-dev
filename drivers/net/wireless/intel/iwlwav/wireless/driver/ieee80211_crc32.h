/******************************************************************************

                                Copyright (c) 2012
                            Lantiq Deutschland GmbH

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
#ifndef IEEE80211_CRC32_H
#define IEEE80211_CRC32_H

u32 ieee80211_crc32(const u8 *frame, size_t frame_len);

#endif /* CRC32_H */
